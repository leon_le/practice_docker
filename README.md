**Express Redis Docker Compose app**
Start redis server 1st.

To start the app run: `docker-compose up`.

It will then be started on port 3000.

---

**Endpoints**

---

## Hello World
`curl http://localhost:3000`
---

## Storing Data
`curl http://localhost:3000/store/my-key\?some\=value\&some-other\=other-value`

---
## Fetching Data
`curl http://localhost:3000/my-key`